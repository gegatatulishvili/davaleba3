package com.example.personalinfoform

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class EmailActivity : AppCompatActivity() {
    lateinit var emailEditText: EditText
    lateinit var emailButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_email)

        val name: String? = intent.extras?.getString("NAME", "NO NAME")
        val age: Int? = intent.extras?.getInt("AGE", 0)

        emailEditText = findViewById(R.id.emailEditText)
        emailButton = findViewById(R.id.nextButtonEmailActivity)

        emailButton.setOnClickListener {
            val email = emailEditText.text.toString()
            if (email.isBlank()){
                Toast.makeText(this, "Input email", Toast.LENGTH_SHORT).show()
            } else {
                val intent = Intent(this, FinishActivity::class.java)
                intent.putExtra("NAME", name)
                intent.putExtra("AGE", age)
                intent.putExtra("EMAIL", email)
                startActivity(intent)

                finish()
            }
        }
    }
}