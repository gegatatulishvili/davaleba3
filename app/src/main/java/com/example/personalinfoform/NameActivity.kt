package com.example.personalinfoform

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class NameActivity : AppCompatActivity() {
    lateinit var nameEditText: EditText
    lateinit var nextButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_name)

        nameEditText = findViewById(R.id.nameEditText)
        nextButton = findViewById(R.id.nextButtonNameActivity)

        nextButton.setOnClickListener {
            val name = nameEditText.text.toString()
            if (name.isBlank()){
                Toast.makeText(this, "Input name", Toast.LENGTH_SHORT).show()
            } else {
                val intent = Intent(this, AgeActivity::class.java)
                intent.putExtra("NAME", name)
                startActivity(intent)

                finish()
            }
        }
    }
}