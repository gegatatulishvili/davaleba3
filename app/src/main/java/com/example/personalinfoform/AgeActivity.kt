package com.example.personalinfoform

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class AgeActivity : AppCompatActivity() {
    lateinit var ageEditText: EditText
    lateinit var ageButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_age)

        val name: String? = intent.extras?.getString("NAME", "NO NAME")

        ageEditText = findViewById(R.id.ageEditText)
        ageButton = findViewById(R.id.nextButtonAgeActivity)

        ageButton.setOnClickListener {
            val age = ageEditText.text.toString().toIntOrNull()
            if (age == null){
                Toast.makeText(this, "Input age", Toast.LENGTH_SHORT).show()
            } else {
                val intent = Intent(this, EmailActivity::class.java)
                intent.putExtra("NAME", name)
                intent.putExtra("AGE", age)
                startActivity(intent)

                finish()
            }
        }
    }
}