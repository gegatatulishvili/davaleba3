package com.example.personalinfoform

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView

class FinishActivity : AppCompatActivity() {
    lateinit var nameTextView: TextView
    lateinit var ageTextView: TextView
    lateinit var emailTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finish)

        nameTextView = findViewById(R.id.nameTextViewFinishActivity)
        ageTextView = findViewById(R.id.ageTextViewFinishActivity)
        emailTextView = findViewById(R.id.emailTextViewFinishActivity)

        val name: String? = intent.extras?.getString("NAME", "NO NAME")
        val age: Int? = intent.extras?.getInt("AGE", 0)
        val email: String? = intent.extras?.getString("EMAIL", "NO EMAIL")

        nameTextView.text = "Name: " + name
        ageTextView.text = "Age: " + age.toString()
        emailTextView.text = "Email: " + email
    }
}